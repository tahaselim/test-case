import { createApp } from 'vue'

import './index.css'
import store from './store'
import router from './router'
import { Plugin as FontAwesomePlugin } from './plugins/FontAwesome'
import { Plugin as SweetAlertPlugin } from './plugins/SweetAlert'

import App from './App.vue'

const app = createApp(App)
	.use(store)
	.use(router)
	.use(FontAwesomePlugin)
	.use(SweetAlertPlugin)

app.mount('#app')
