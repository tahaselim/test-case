import { inject } from 'vue'
import Swal from 'sweetalert2'

export const SwalSymbol = Symbol('sweetalert2')
export const ToastSymbol = Symbol('sweetalert2-toast')

export function useSwal() {
	return inject(SwalSymbol)
}

export function useToast() {
	return inject(ToastSymbol)
}

export const Toast = Swal.mixin({
	timer: 2000,
	toast: true,
	icon: 'success',
	position: 'top',
	timerProgressBar: true,
	showConfirmButton: false,
})
